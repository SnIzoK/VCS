$(".slider").slick({
  infinite: true,
  dots: false,
  arrows: false,
  autoplay: true,
});

$(".how-is-works-slider").slick({
	arrows: true,
  	autoplay: false,
  	dots: true,
  	nextArrow: document.getElementById("NextArrow"),
  	prevArrow: document.getElementById("PrevArrow"),
});